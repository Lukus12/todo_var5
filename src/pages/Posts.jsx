import React, {useEffect, useState} from 'react';
import '../styles/App.css'
import PostService from "../API/PostService";//принимает посты с серва
import {usePosts} from "../hooks/usePosts";//сортровка постов
import {useFetching} from "../hooks/useFetching";//обработка ошибок и вывод разрузочной анимации
import {getPageCount} from "../utils/pages";//разметка
import MyButton from "../components/UI/button/MyButton";//своя кнопка
import PostForm from "../components/PostForm";//общая форма поста
import MyModal from "../components/UI/MyModal/MyModal";//модальное окно
import PostFilter from "../components/PostFilter";//фильтр
import PostList from "../components/PostList";//общий пост
import Loader from "../components/UI/Loader/Loader";//анимация заргузки
import Pagination from "../components/UI/pagination/Pagination";
import MySelect from "../components/UI/select/MySelect";//связь с фильтром


function Posts() {
    const [posts, setPosts] = useState([])
    const [filter, setFilter] = useState({sort: '', query: ''})
    const [modal, setModal] = useState(false);
    const [totalPages, setTotalPages] = useState(0);
    const [limit, setLimit] = useState(10);
    const [page, setPage] = useState(1);
    const sortedAndSearchedPosts = usePosts(posts, filter.sort, filter.query);

    const [fetchPosts, isPostsLoading, postError] = useFetching(async (limit, page) => {
        const response = await PostService.getAll(limit, page);
        setPosts(response.data)
        const totalCount = response.headers['x-total-count']
        setTotalPages(getPageCount(totalCount, limit));
    })


    useEffect(() => {//подгрузка постов с серва
        fetchPosts(limit, page)
    }, [limit, page])

    const createPost = (newPost) => {
        setPosts([...posts, newPost])
        setModal(false)
    }

    // Получаем post из дочернего компонента
    const removePost = (post) => {
        setPosts(posts.filter(p => p.id !== post.id))
    }

    const changePage = (page) => {
        setPage(page)
        fetchPosts(limit, page)
    }

    return (
        <div className="App">
            <MyButton style={{marginTop: 30}} onClick={() => setModal(true)}>
                Создать пост TODO
            </MyButton>
            <MyModal visible={modal} setVisible={setModal}>
                <PostForm create={createPost}/>
            </MyModal>
            <hr style={{margin: '15px 0'}}/>
            <PostFilter
                filter={filter}
                setFilter={setFilter}
            />
            <MySelect
                value={limit}
                onChange={value => setLimit(value)}
                defaultValue="Кол-во элементов на странице"
                options={[
                    {value: 5, name: '5'},
                    {value: 10, name: '10'},
                    {value: 25, name: '25'},
                    {value: -1, name: 'Показать все'},
                ]}
            />
            {postError &&
            <h1>Произошла ошибка ${postError}</h1>
            }
            {isPostsLoading
                ?<div style={{display: 'flex', justifyContent: 'center', marginTop: 50}}><Loader/></div>
                :<PostList remove={removePost} posts={sortedAndSearchedPosts} title="Посты TODO"/>
            }
            <Pagination
                page={page}
                changePage={changePage}
                totalPages={totalPages}
            />
        </div>
    );
}

export default Posts;
